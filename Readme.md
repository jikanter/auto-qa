# Stratigent Auto-QA projects

download docker-ci-toolkit by running the following the top level 
directory. It is needed in order to fork off remote processes from the local
host and deploy. 
$ git clone https://github.com/marcelbirkner/docker-ci-tool-stack.git

## On the mac

Since most people already have chrome installed, I install to ${HOME}/Developer/Applications.

$ brew cask install --appdir=${HOME}/Developer/Applications google-chrome-dev

See https://intoli.com/blog/running-selenium-with-headless-chrome/

  $ PLATFORM=mac64
  $ VERSION=$(curl http://chromedriver.storage.googleapis.com/LATEST_RELEASE)
  curl http://chromedriver.storage.googleapis.com/$VERSION/chromedriver_$PLATFORM.zip \
  | bsdtar -xvf - -C env/bin/

