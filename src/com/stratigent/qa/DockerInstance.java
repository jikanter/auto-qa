package com.stratigent.qa;

import java.net.*;
import java.util.concurrent.*;

/**
 * Created by Jordan.Kanter on 8/4/2016.
 * This class manages the instantiation of a remote
 * docker instance, and forking them off as necessary
 */
public class DockerInstance {

  // container URL
  URL dockerURL;

  public DockerInstance() {
    try {
      this.dockerURL = new URL("http://localhost:9090");
    } catch (MalformedURLException e) {

    }

  }

  public URL getUrl() {
    return dockerURL;
  }

}
