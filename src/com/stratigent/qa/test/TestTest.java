package com.stratigent.qa.test;

import com.stratigent.qa.*;
import org.junit.*;
import org.junit.Assert.*;

import java.lang.annotation.Annotation;
import java.util.*;

/**
 * Created by Jordan.Kanter on 9/12/2016.
 *
 */
public class TestTest implements Test {

  ArrayList<String> tcases = new ArrayList<String>();
  TestController t = new TestController("chrome", tcases);

  public Class<? extends Throwable> expected() {
    return null;
  }

  public long timeout() { return 0; }

  public Class<? extends Annotation> annotationType() {
    return null;
  }

}
