package com.stratigent.qa;

import java.util.Properties;
import java.io.*;

/**
 * Created by Jordan.Kanter on 5/18/2016.
 */
public class SeConfiguration {

  public Properties configurationProperties;
  private static SeConfiguration instance = null;

  public static SeConfiguration getInstance() {
    if (instance == null) {
      instance = new SeConfiguration();

    }
    return instance;
  }

  public SeConfiguration() {
    try {
      // TODO: figure out how to read from a configuration file
      FileInputStream reader = new FileInputStream(new File("configuration.properties.xml"));
      System.out.println(reader);
      this.configurationProperties = new Properties();
      this.configurationProperties.loadFromXML(reader);
    } catch (java.io.IOException e) {
      System.err.println("could not read properties file: " + e.toString());
    }
  }

  static void initRemoteContainer() {}

  /*
   * sets up a remote jenkins and loads the persistant javascript
   * libraries in.
   */
  void initRemoteHost() {
    // this function sets up jenkins and loads the javascript libraries into
    WebJenkinsController c = new WebJenkinsController();
    try {
      c.setURI(new java.net.URI(this.configurationProperties.getProperty("jenkins.url") + "/computer/new"));
    } catch (java.net.URISyntaxException e) {
      System.err.println("url syntax exception: " + e.toString());
    }
  }

}
