package com.stratigent.qa;

import java.io.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class SeTestMain {

    public static boolean utilizesRc = false;
    public static boolean autoConfigureDrivers = true;
    static boolean localOnly = true;

    public TestController tc;

    String path;
    static WebDriver driver;



    public static void main(String[] args) {
        String browser = args[0];
        String browserVersion = args[1];
        String baseUrl = args[2];
        String testCaseFile = args[3];

        try {
            if (browser.equals("chrome")) {
                SeConfiguration conf = SeConfiguration.getInstance();
                System.out.println(conf.configurationProperties.getProperty(("stratigent.qa.chrome.driver")));
                //System.setProperty("webdriver.chrome.driver", conf.configurationProperties.getProperty("stratigent.qa.chrome.driver"));
                driver = new ChromeDriver();

            } else if (browser.equals("firefox")) {
                driver = new FirefoxDriver();
            } else if (browser.equals("safari")) {
                throw new UnsupportedBrowserException();
            } else if (browser.equals("ie")) {
                throw new UnsupportedBrowserException();
            }
        } catch (UnsupportedBrowserException e) {
            System.err.println("Unsupported browser requested.... Exiting");
            System.exit(-1);
        }

        // initialize the entire test framework given a testCaseFile
        if (testCaseFile.length() > 0) {
            new TestController(browser, new File(testCaseFile));
        }

    }


}