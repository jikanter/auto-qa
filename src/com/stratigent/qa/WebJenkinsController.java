package com.stratigent.qa;

import com.thoughtworks.selenium.webdriven.JavascriptLibrary;
import org.apache.http.client.methods.*;
import java.net.URI;

/**
 * Created by Jordan.Kanter on 8/23/2016.
 */
public class WebJenkinsController extends JavascriptLibrary {

  public HttpGet getUtility = new HttpGet();

  void setURI(URI u) {
        getUtility.setURI(u);
    }

}
