package com.stratigent.qa;

import junit.extensions.TestSetup;
import junit.framework.Test;
import junit.framework.TestResult;
import org.apache.http.HttpRequest;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.io.*;
import java.util.List;
import java.util.Properties;

/**
 * Created by Jordan.Kanter on 6/15/2016.
 * Class for running of test cases via an API (Programmatically)
 */

public class TestController
  extends WebJenkinsController
  implements Test {

    /*
     * A test that can run other tests
     * @param testContent: javascript test content
     */

  String browserType;
  Test[] testStack;
  List<String> testCases;
  String clientId;
  public static TestSetup testConfig;

  public String hostLanguage;
  public String defaultHostBrowser;
  public Properties configurationProperties;

  public TestController(String browserType, List<java.lang.String> testCases) {
    new TestController(browserType, testCases, null);
  }

  public TestController(String browserType, File testCaseFile) {
    this.configurationProperties = new SeConfiguration().configurationProperties;
    this.configurationProperties.getProperty("seleniumqa.hostlanguage");
    this.defaultHostBrowser = this.configurationProperties.getProperty("seleniumqa.defaults.jshost");
    String line = null;
    try {
      FileReader fileReader = new FileReader(testCaseFile);
      BufferedReader bufferedReader = new BufferedReader(fileReader);
      while ((line = bufferedReader.readLine()) != null) {
        testCases.add(testCases.size() + 1, line);
      }

    } catch (FileNotFoundException e) {

    } catch (IOException e) {
    }
    new TestController(browserType, testCases, null);

  }

  public TestController(String browserType, List<java.lang.String> testCases, String clientId) {
    if (!hostLanguage.equals("javascript")) {
      throw new Error("only javascript is supported at this time!");
    }

    this.browserType = browserType;
    this.testCases = testCases;
    this.clientId = (clientId != null) ? clientId : "stratigent";
  }

  public int countTestCases() {
    return (this.testCases.size() > 0 ? this.testCases.size() : 0);
  }

  public void run(TestResult testResult) {

  }

  /* the goal here is to run arbitrary javascript tests */
    /* run the javascript test within the context of webdriver */
    /* see http://www.seleniumhq.org/docs/03_webdriver.jsp#using-javascript */
  public static WebElement run(WebDriver driver, String jsTest) {
    if (!jsTest.endsWith(";")) {
      return null;
    }
    WebElement element = (WebElement) ((JavascriptExecutor) driver).executeScript(jsTest);
    return element;
  }

}
