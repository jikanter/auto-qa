#!/usr/bin/env ruby -d
# You may need to run java -jar selenium-server-standalone-2.50.1.jar
# from the current directory for this client script to run
require 'rubygems'
require 'selenium-webdriver'


selenium_host = ENV['SELENIUM_HOST'] || 'localhost'
selenium_port = ENV['SELENIUM_PORT'] || '4444'
selenium_platform = ENV['SELENIUM_PLATFORM'] || 'ANY'
selenium_browser = ENV['SELENIUM_BROWSER'] || ARGV.shift
selenium_browser_version = ENV['SELENIUM_VERSION']
selenium_chrome_driver_path = ENV['SELENIUM_CHROME_DRIVER_PATH'] || 'lib/env/bin' + selenium_lib_path

bs_username = ENV['BS_USERNAME']
bs_key = ENV['BS_AUTHKEY']
bs_project = ENV['BS_AUTOMATE_PROJECT']
bs_build = ENV['BS_AUTOMATE_BUILD']

url = "http://" + selenium_host + ":" + selenium_port + "/wd/hub"

if selenium_browser == 'chrome'
  puts "Testing selenium with hub " + url + " and browser " + selenium_browser
  capabilities = Selenium::WebDriver::Remote::Capabilities.new
  if selenium_host == 'localhost'
    capabilities['webdriver.chrome.driver'] = selenium_chrome_driver_path
  end
  capabilities['platform'] = selenium_platform
  capabilities['browserName'] = selenium_browser
  capabilities['version'] = selenium_browser_version
  capabilities['browserstack.user'] = bs_username
  capabilities['browserstack.key'] = bs_key
  capabilities['project'] = bs_project
  capabilities['build'] = bs_build
  # see http://selenium.googlecode.com/git/docs/api/rb/Selenium/WebDriver.html for API docs
  driver = Selenium::WebDriver.for(:remote, :url => url, :desired_capabilities => capabilities)

  # TODO: run a search
  # driver.navigate.to "http://stage.united.com/ual"
  # orig = driver.find_element(:id, 'Origin')
  # orig.send_keys "ORD"
  # orig.submit
  # dest = driver.find_element(:id, "Destination")
  # dest.send_keys "LAX"
  # depart_date = driver.find_element(:id, "DepartDate")
  # depart_date.send_keys "Mar 11, 2016"
  # return_date = driver.find_element(:id, "ReturnDate")
  # return_date.send_keys "Apr 15, 2016"
  # search_button = driver.find_element(:name, "search")
  # search_button.click

  # runTestCaseOnCurrentPageAndCheck(driver.title, 'Flight Search Results | United Airlines')
  driver.navigate.to "http://stage.united.com/ual"
  orig = driver.find_element(:id, "Origin")
  orig.send_keys "ORD"
  #runTestCaseOnCurrentPageAndCheck(driver.title)

  driver.quit
elsif selenium_browser == 'firefox'

end
