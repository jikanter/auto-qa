require "json"
require "selenium-webdriver"
gem "test-unit"
require "test/unit"

selenium_browser = "firefox"
BASE_URL = ENV["BASE_URL"] || "https://stage.united.com/ual/"

class SeTestFirefox < Test::Unit::TestCase

  def setup
    @driver = Selenium::WebDriver.for :firefox
    @base_url = BASE_URL
    @accept_next_alert = true
    @driver.manage.timeouts.implicit_wait = 30
    @verification_errors = []
  end

  def teardown
    @driver.quit
    assert_equal [], @verification_errors
  end

  def test_se
    @driver.get(@base_url)
    @driver.navigate.refresh
    assert_equal "United Airlines – Airline Tickets, Travel Deals and Flights", @driver.title
  end

end
#
#   def test_se
#     @driver.get "https://stage.united.com/ual/en/us"
#     # Clear the SearchInput Cookie so it does not autopopulate
#     # ERROR: Caught exception [ERROR: Unsupported command [deleteCookie | SearchInput | path=/, domain=.united.com]]
#     @driver.navigate.refresh
#     assert_equal "United Airlines – Airline Tickets, Travel Deals and Flights", @driver.title
#     # Validate UA Data Layer
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.hasOwnProperty("UA") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.hasOwnProperty("Utilities") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.Utilities === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.hasOwnProperty("Localize") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.Localize === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.hasOwnProperty("AppData") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.hasOwnProperty("Junk123") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.hasOwnProperty("Search") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.hasOwnProperty("UI") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.UI === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.hasOwnProperty("Common") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.Common === "object") | ]]
#     # Validate the critical AppData
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.hasOwnProperty("Data") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.hasOwnProperty("Page") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Page === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Page.hasOwnProperty("ClientID") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Page.ClientID === "string") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.hasOwnProperty("Session") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Session.hasOwnProperty("Timeout") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Session.Timeout === 'number') | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | false;// this fails because of a bad data layer : window.UA.AppData.Data.Session.hasOwnProperty("Profile")  | ]]
#     # Make sure that we have set the login status in local storage
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.localStorage.hasOwnProperty("loginStatus") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.localStorage.loginStatus == "string") | ]]
#     # Validate some UA Basics
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Session.Url === "string") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Session.Url | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Session.UrlReferrer === "string") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Session.UrlReferrer | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Session.LanguageCulture === "string") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Session.LanguageCulture == "en" | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Session.Currency === "string") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Session.Currency == "USD" | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Session.POS === "string") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Session.POS == "US" | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Session.UaSessionId === "string") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Session.UaSessionId.length | ]]
#     # Make sure UaSessionId contains 4 dashes
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Session.UaSessionId.match(/-/g).length | ]]
#     # Make sure the Server Timestamp conforms to /Date(<blah>)/
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Page.Timestamp === "string") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Page.Timestamp.split('/').length -1 | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof eval('new ' + window.UA.AppData.Data.Page.Timestamp.slice(1,window.UA.AppData.Data.Page.Timestamp.length-1)) === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.Homepage.hasOwnProperty("Tiles") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.Bootstrapper.hasOwnProperty("dataObject") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.Bootstrapper.dataObject.fooBarAbcE == 'undefined') | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.Bootstrapper.dataObject.hasOwnProperty("getData") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.Bootstrapper.dataObject.getData === "function") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.Bootstrapper.dataObject.hasOwnProperty("global") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.Bootstrapper.dataObject.hasOwnProperty("page") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.Bootstrapper.dataObject.hasOwnProperty("pulse") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.Bootstrapper.dataObject.dataLayerLoaded | ]]
#     # Find Search Page Objects then run a search
#     # ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
#     # ERROR: Caught exception [ERROR: Unsupported command [isEditable | Origin | ]]
#     # ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
#     # ERROR: Caught exception [ERROR: Unsupported command [isEditable | Destination | ]]
#     # ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
#     # ERROR: Caught exception [ERROR: Unsupported command [isEditable | DepartDate | ]]
#     # ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
#     # ERROR: Caught exception [ERROR: Unsupported command [isEditable | ReturnDate | ]]
#     # ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
#     # ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
#     # ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
#     # ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
#     # ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
#     assert_equal "Flight Search Results | United Airlines", @driver.title
#     # Validate some search results data
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (window.location.pathname.indexOf('/flight-search') > -1 == true) | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Search === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search == null | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.hasOwnProperty("Origin") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.Origin | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.hasOwnProperty("Destination") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.Destination | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Search.DepartDate === "string") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.DepartDate | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.ReturnDate | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.SearchMethod | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.CabinType | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.Flexible | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.CarrierPref | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.PortOxygen | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.TravelerCount | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Search.TravelerTypes === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Search.TravelerTypes.numAdults === "number") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.TravelerTypes.numAdults | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Search.TravelerTypes.numChildren2to4 === "number") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.TravelerTypes.numChildren2to4 | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Search.TravelerTypes.numChildren5to11 === "number") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.TravelerTypes.numChildren5to11 | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Search.TravelerTypes.numChildren12to17 === "number") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.TravelerTypes.numChildren12to17 | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Search.TravelerTypes.numChildren16to17 === "number") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.TravelerTypes.numChildren16to17 | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Search.TravelerTypes.numInfants === "number") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.TravelerTypes.numInfants | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Search.TravelerTypes.numLapInfants === "number") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.TravelerTypes.numLapInfants | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Search.TravelerTypes.numSeniors === "number") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Search.TravelerTypes.numSeniors | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.Bootstrapper.dataObject.page.srchRslts === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.Bootstrapper.dataObject.page.srchRslts.contentShown | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.Bootstrapper.dataObject.page.srchRslts.pageIndex === "number") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.Booking.FlightSearch === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.Booking.FlightSearch.currentResults === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.Booking.FlightSearch.currentResults.appliedSearch === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | true;// broken data layer (typeof window.UA.Booking.FlightSearch.currentResults.IsAward === "boolean") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | false;; // broken data layer window.UA.Booking.FlightSearch.currentResults.IsAward | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.Booking.FlightSearch.currentResults.IsListView === "boolean") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.Booking.FlightSearch.currentResults.IsListView | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | true; // fails because of bad data layer (typeof window.UA.Booking.FlightSearch.currentResults.HideNonStopOnly === "boolean") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | false; // fails because of bad data layer window.UA.Booking.FlightSearch.currentResults.HideNonStopOnly | ]]
#     # Make sure this is a revenue page
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | document.querySelectorAll("#hdnAward").length | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.Bootstrapper.dataObject.page.pageType | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.Booking.FlightSearch.currentResults === 'object') | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.Booking.FlightSearch.currentResults === 'object') | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.Booking.FlightSearch.currentResults.SearchFilters === "object") | ]]
#     # Click the first two economy fares and get to RTI
#     # ERROR: Caught exception [ERROR: Unsupported command [waitForCondition | selenium.browserbot.getCurrentWindow().document.querySelectorAll('.flight-result-list').length > 0 | 500000]]
#     @driver.find_element(:xpath, "//*[contains(@id,'product_ECONOMY')]/a").click
#     # Wait until the second leg
#     # ERROR: Caught exception [ERROR: Unsupported command [waitForCondition | selenium.browserbot.getCurrentWindow().document.querySelectorAll('.flight-result-list').length > 0 | 500000]]
#     @driver.find_element(:xpath, "//*[contains(@id,'product_ECONOMY')]/a").click
#     # Validate Data Layer on RTI
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Cart === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | (typeof window.UA.AppData.Data.Cart.Products === "object") | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Cart.Products.length > 0 | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | parseFloat(window.UA.AppData.Data.Cart.TotalTaxes) > 0 | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | parseFloat(window.UA.AppData.Data.Cart.TotalTransactionCost) > 0 | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | parseFloat(window.UA.AppData.Data.Cart.TotalTransactionCostUSD) > 0 | ]]
#     # ERROR: Caught exception [ERROR: Unsupported command [getEval | window.UA.AppData.Data.Cart.TransactionType | ]]
#     # Do a one-way search
#     @driver.get "https://stage.united.com/ual/en/us"
#     # Clear the SearchInput Cookie so it does not autopopulate
#     # ERROR: Caught exception [ERROR: Unsupported command [deleteCookie | SearchInput | path=/, domain=.united.com]]
#     @driver.navigate.refresh
#     assert_equal "United Airlines – Airline Tickets, Travel Deals and Flights", @driver.title
#     # Do a one-way booking
#     @driver.find_element(:id, "SearchTypeMain_oneWay").click
#     # ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
#     # ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
#     # ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
#     # ERROR: Caught exception [Error: locator strategy either id or name must be specified explicitly.]
#   end
#
#   def element_present?(how, what)
#     ${receiver}.find_element(how, what)
#     true
#   rescue Selenium::WebDriver::Error::NoSuchElementError
#     false
#   end
#
#   def alert_present?()
#     ${receiver}.switch_to.alert
#     true
#   rescue Selenium::WebDriver::Error::NoAlertPresentError
#     false
#   end
#
#   def verify(&blk)
#     yield
#   rescue Test::Unit::AssertionFailedError => ex
#     @verification_errors << ex
#   end
#
#   def close_alert_and_get_its_text(how, what)
#     alert = ${receiver}.switch_to().alert()
#     alert_text = alert.text
#     if (@accept_next_alert) then
#       alert.accept()
#     else
#       alert.dismiss()
#     end
#     alert_text
#   ensure
#     @accept_next_alert = true
#   end
# end
