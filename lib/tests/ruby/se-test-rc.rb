#!/usr/bin/env ruby
require "test/unit"
require "rubygems"
# needs a gem install selenium-client
gem "selenium-client"
require "selenium/client"

# After emulator testing, implement
# implement window.IMG with our custom preamble, then we can capture all requests via a our custom
#           window.IFRAME with our custom preamble
#
# then we can capture a test path with all the pixels
# if we give it a list of pixels to check, at the very last
#
# if you build a site that contains an overridden pixel, then we can build
# into that template generic callback functions that whenever that
# img is called back the per client, and then you have a generic
# solution that can substitute, then you copy the callback function
# into the selenium output
# On every page it contains a callback for the data layer
# Then we have some junior level resource writing the dlg for

BASE_URL = ENV["BASE_URL"] || "https://stage.united.com/ual"

class SeUtils

  def self.send_keys(se, locator, value)
    se.type_keys(locator, value)
  end

end

class SeTestRc < Test::Unit::TestCase

  def self.send_keys(se, locator, value)
    se.type_keys(locator, value)
  end

  def setup
    @verification_errors = []
    @selenium = Selenium::Client::Driver.new(
      :host => "localhost",
      :port => 4444,
      :browser => "*firefox",
      :url => "https://stage.united.com/ual/",
      :timeout_in_second => 60)

    @selenium.start_new_browser_session
  end

  def teardown
    @selenium.close_current_browser_session
    assert_equal [], @verification_errors
  end

  def test_se
    encoding = "utf-8"
    @selenium.open "https://stage.united.com/ual/en/us"
    # Clear the SearchInput Cookie so it does not autopopulate
    @selenium.delete_cookie "SearchInput", "path=/, domain=.united.com"
    @selenium.refresh
    @selenium.wait_for_page_to_load "30000"
    assert_equal "United Airlines \u2013 Airline Tickets, Travel Deals and Flights", @selenium.get_title.force_encoding(encoding)
    # Validate UA Data Layer
    assert_equal "true", @selenium.get_eval("window.hasOwnProperty(\"UA\")")
    assert_equal "true", @selenium.get_eval("window.UA.hasOwnProperty(\"Utilities\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.Utilities === \"object\")")
    assert_equal "true", @selenium.get_eval("window.UA.hasOwnProperty(\"Localize\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.Localize === \"object\")")
    assert_equal "true", @selenium.get_eval("window.UA.hasOwnProperty(\"AppData\")")
    assert_equal "false", @selenium.get_eval("window.UA.hasOwnProperty(\"Junk123\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData === \"object\")")
    assert_equal "true", @selenium.get_eval("window.UA.AppData.Data.hasOwnProperty(\"Search\")")
    assert_equal "null", @selenium.get_eval("window.UA.AppData.Data.Search")
    assert_equal "true", @selenium.get_eval("window.UA.hasOwnProperty(\"UI\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.UI === \"object\")")
    assert_equal "true", @selenium.get_eval("window.UA.hasOwnProperty(\"Common\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.Common === \"object\")")
    # Validate the critical AppData
    assert_equal "true", @selenium.get_eval("window.UA.AppData.hasOwnProperty(\"Data\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data === \"object\")")
    assert_equal "true", @selenium.get_eval("window.UA.AppData.Data.hasOwnProperty(\"Page\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Page === \"object\")")
    assert_equal "true", @selenium.get_eval("window.UA.AppData.Data.Page.hasOwnProperty(\"ClientID\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Page.ClientID === \"string\")")
    assert_equal "true", @selenium.get_eval("window.UA.AppData.Data.hasOwnProperty(\"Session\")")
    assert_equal "true", @selenium.get_eval("window.UA.AppData.Data.Session.hasOwnProperty(\"Timeout\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Session.Timeout === 'number')")
    assert_equal "false", @selenium.get_eval("false;// this fails because of a bad data layer : window.UA.AppData.Data.Session.hasOwnProperty(\"Profile\")")
    # Make sure that we have set the login status in local storage
    assert_equal "true", @selenium.get_eval("window.localStorage.hasOwnProperty(\"loginStatus\")")
    assert_equal "true", @selenium.get_eval("(typeof window.localStorage.loginStatus == \"string\")")
    # Validate some UA Basics
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Session.Url === \"string\")")
    assert_equal "http://stage.united.com/ual/en/us/default/home/clientdata", @selenium.get_eval("window.UA.AppData.Data.Session.Url")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Session.UrlReferrer === \"string\")")
    assert_equal "https://stage.united.com/ual/en/us", @selenium.get_eval("window.UA.AppData.Data.Session.UrlReferrer")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Session.LanguageCulture === \"string\")")
    assert_equal "true", @selenium.get_eval("window.UA.AppData.Data.Session.LanguageCulture == \"en\"")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Session.Currency === \"string\")")
    assert_equal "true", @selenium.get_eval("window.UA.AppData.Data.Session.Currency == \"USD\"")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Session.POS === \"string\")")
    assert_equal "true", @selenium.get_eval("window.UA.AppData.Data.Session.POS == \"US\"")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Session.UaSessionId === \"string\")")
    assert_equal "36", @selenium.get_eval("window.UA.AppData.Data.Session.UaSessionId.length")
    # Make sure UaSessionId contains 4 dashes
    assert_equal "4", @selenium.get_eval("window.UA.AppData.Data.Session.UaSessionId.match(/-/g).length")
    # Make sure the Server Timestamp conforms to /Date(<blah>)/
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Page.Timestamp === \"string\")")
    assert_equal "2", @selenium.get_eval("window.UA.AppData.Data.Page.Timestamp.split('/').length -1")
    assert_equal "true", @selenium.get_eval("(typeof eval('new ' + window.UA.AppData.Data.Page.Timestamp.slice(1,window.UA.AppData.Data.Page.Timestamp.length-1)) === \"object\")")
    assert_equal "true", @selenium.get_eval("window.UA.Homepage.hasOwnProperty(\"Tiles\")")
    assert_equal "true", @selenium.get_eval("window.Bootstrapper.hasOwnProperty(\"dataObject\")")
    assert_equal "true", @selenium.get_eval("(typeof window.Bootstrapper.dataObject.fooBarAbcE == 'undefined')")
    assert_equal "true", @selenium.get_eval("window.Bootstrapper.dataObject.hasOwnProperty(\"getData\")")
    assert_equal "true", @selenium.get_eval("(typeof window.Bootstrapper.dataObject.getData === \"function\")")
    assert_equal "true", @selenium.get_eval("window.Bootstrapper.dataObject.hasOwnProperty(\"global\")")
    assert_equal "true", @selenium.get_eval("window.Bootstrapper.dataObject.hasOwnProperty(\"page\")")
    assert_equal "true", @selenium.get_eval("window.Bootstrapper.dataObject.hasOwnProperty(\"pulse\")")
    assert_equal "1", @selenium.get_eval("window.Bootstrapper.dataObject.dataLayerLoaded")
    # Find Search Page Objects then run a search
    assert @selenium.is_element_present("Origin")
    assert @selenium.is_editable("Origin")
    assert @selenium.is_element_present("Destination")
    assert @selenium.is_editable("Destination")
    assert @selenium.is_element_present("DepartDate")
    assert @selenium.is_editable("DepartDate")
    assert @selenium.is_element_present("ReturnDate")
    assert @selenium.is_editable("ReturnDate")
    sleep 10
    SeUtils.send_keys(@selenium, "Origin", "ORD")
    SeUtils.send_keys(@selenium, "Destination", "SFO")
    SeUtils.send_keys(@selenium, "DepartDate", "10/28/2016")
    SeUtils.send_keys(@selenium, "ReturnDate", "11/1/2016")
    @selenium.click "search"
    @selenium.wait_for_page_to_load "30000"
    assert_equal "Flight Search Results | United Airlines", @selenium.get_title
    # Validate some search results data
    assert_equal "true", @selenium.get_eval("(window.location.pathname.indexOf('/flight-search') > -1 == true)")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Search === \"object\")")
    assert_equal "false", @selenium.get_eval("window.UA.AppData.Data.Search == null")
    assert_equal "true", @selenium.get_eval("window.UA.AppData.Data.Search.hasOwnProperty(\"Origin\")")
    assert_equal "ORD", @selenium.get_eval("window.UA.AppData.Data.Search.Origin")
    assert_equal "true", @selenium.get_eval("window.UA.AppData.Data.Search.hasOwnProperty(\"Destination\")")
    assert_equal "SFO", @selenium.get_eval("window.UA.AppData.Data.Search.Destination")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Search.DepartDate === \"string\")")
    assert_equal "Oct 28, 2016", @selenium.get_eval("window.UA.AppData.Data.Search.DepartDate")
    assert_equal "Nov 01, 2016", @selenium.get_eval("window.UA.AppData.Data.Search.ReturnDate")
    assert_equal "roundTrip", @selenium.get_eval("window.UA.AppData.Data.Search.SearchMethod")
    assert_equal "econ", @selenium.get_eval("window.UA.AppData.Data.Search.CabinType")
    assert_equal "false", @selenium.get_eval("window.UA.AppData.Data.Search.Flexible")
    assert_equal "CPAll", @selenium.get_eval("window.UA.AppData.Data.Search.CarrierPref")
    assert_equal "nPortOx", @selenium.get_eval("window.UA.AppData.Data.Search.PortOxygen")
    assert_equal "1", @selenium.get_eval("window.UA.AppData.Data.Search.TravelerCount")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Search.TravelerTypes === \"object\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Search.TravelerTypes.numAdults === \"number\")")
    assert_equal "1", @selenium.get_eval("window.UA.AppData.Data.Search.TravelerTypes.numAdults")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Search.TravelerTypes.numChildren2to4 === \"number\")")
    assert_equal "0", @selenium.get_eval("window.UA.AppData.Data.Search.TravelerTypes.numChildren2to4")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Search.TravelerTypes.numChildren5to11 === \"number\")")
    assert_equal "0", @selenium.get_eval("window.UA.AppData.Data.Search.TravelerTypes.numChildren5to11")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Search.TravelerTypes.numChildren12to17 === \"number\")")
    assert_equal "0", @selenium.get_eval("window.UA.AppData.Data.Search.TravelerTypes.numChildren12to17")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Search.TravelerTypes.numChildren16to17 === \"number\")")
    assert_equal "0", @selenium.get_eval("window.UA.AppData.Data.Search.TravelerTypes.numChildren16to17")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Search.TravelerTypes.numInfants === \"number\")")
    assert_equal "0", @selenium.get_eval("window.UA.AppData.Data.Search.TravelerTypes.numInfants")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Search.TravelerTypes.numLapInfants === \"number\")")
    assert_equal "0", @selenium.get_eval("window.UA.AppData.Data.Search.TravelerTypes.numLapInfants")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Search.TravelerTypes.numSeniors === \"number\")")
    assert_equal "0", @selenium.get_eval("window.UA.AppData.Data.Search.TravelerTypes.numSeniors")
    assert_equal "true", @selenium.get_eval("(typeof window.Bootstrapper.dataObject.page.srchRslts === \"object\")")
    assert_equal "results", @selenium.get_eval("window.Bootstrapper.dataObject.page.srchRslts.contentShown")
    assert_equal "true", @selenium.get_eval("(typeof window.Bootstrapper.dataObject.page.srchRslts.pageIndex === \"number\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.Booking.FlightSearch === \"object\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.Booking.FlightSearch.currentResults === \"object\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.Booking.FlightSearch.currentResults.appliedSearch === \"object\")")
    assert_equal "true", @selenium.get_eval("true;// broken data layer (typeof window.UA.Booking.FlightSearch.currentResults.IsAward === \"boolean\")")
    assert_equal "false", @selenium.get_eval("false;; // broken data layer window.UA.Booking.FlightSearch.currentResults.IsAward")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.Booking.FlightSearch.currentResults.IsListView === \"boolean\")")
    assert_equal "true", @selenium.get_eval("window.UA.Booking.FlightSearch.currentResults.IsListView")
    assert_equal "true", @selenium.get_eval("true; // fails because of bad data layer (typeof window.UA.Booking.FlightSearch.currentResults.HideNonStopOnly === \"boolean\")")
    assert_equal "false", @selenium.get_eval("false; // fails because of bad data layer window.UA.Booking.FlightSearch.currentResults.HideNonStopOnly")
    # Make sure this is a revenue page
    assert_equal "0", @selenium.get_eval("document.querySelectorAll(\"#hdnAward\").length")
    assert_equal "revenue", @selenium.get_eval("window.Bootstrapper.dataObject.page.pageType")
    assert !60.times{ break if ("true" == @selenium.get_eval("(typeof window.UA.Booking.FlightSearch.currentResults === 'object')") rescue false); sleep 1 }
    assert_equal "true", @selenium.get_eval("(typeof window.UA.Booking.FlightSearch.currentResults === 'object')")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.Booking.FlightSearch.currentResults.SearchFilters === \"object\")")
    # Click the first two economy fares and get to RTI
    @selenium.wait_for_condition "selenium.browserbot.getCurrentWindow().document.querySelectorAll('.flight-result-list').length > 0", "500000"
    @selenium.click "//*[contains(@id,'product_ECONOMY')]/a"
    @selenium.wait_for_page_to_load "30000"
    # Wait until the second leg
    @selenium.wait_for_condition "selenium.browserbot.getCurrentWindow().document.querySelectorAll('.flight-result-list').length > 0", "500000"
    @selenium.click "//*[contains(@id,'product_ECONOMY')]/a"
    @selenium.wait_for_page_to_load "30000"
    # Validate Data Layer on RTI
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Cart === \"object\")")
    assert_equal "true", @selenium.get_eval("(typeof window.UA.AppData.Data.Cart.Products === \"object\")")
    assert_equal "true", @selenium.get_eval("window.UA.AppData.Data.Cart.Products.length > 0")
    assert_equal "true", @selenium.get_eval("parseFloat(window.UA.AppData.Data.Cart.TotalTaxes) > 0")
    assert_equal "true", @selenium.get_eval("parseFloat(window.UA.AppData.Data.Cart.TotalTransactionCost) > 0")
    assert_equal "true", @selenium.get_eval("parseFloat(window.UA.AppData.Data.Cart.TotalTransactionCostUSD) > 0")
    assert_equal "Revenue", @selenium.get_eval("window.UA.AppData.Data.Cart.TransactionType")
    # Do a one-way search
    @selenium.open "https://stage.united.com/ual/en/us"
    # Clear the SearchInput Cookie so it does not autopopulate
    @selenium.delete_cookie "SearchInput", "path=/, domain=.united.com"
    @selenium.refresh
    @selenium.wait_for_page_to_load "30000"
    # This is equivalent to '-' in 8-bit ASCII
    # TODO: force utf-8
    assert_equal "United Airlines \xE2\x80\x93 Airline Tickets, Travel Deals and Flights", @selenium.get_title
    sleep 1
    # Do a one-way booking
    @selenium.click "id=SearchTypeMain_oneWay"
    sleep 1
    SeUtils.send_keys(@selenium,  "Origin", "ORD")
    SeUtils.send_keys(@selenium, "Destination", "SFO")
    SeUtils.send_keys(@selenium, "DepartDate", "10/28/2016")
    @selenium.click "search"
    @selenium.wait_for_page_to_load "30000"
  end
end
