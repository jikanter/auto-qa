/* stub the window for execution from node */
if ((typeof window === 'undefined') && (typeof global === 'object')) { 
  self = window = global;
}



/* implement internal browser API */
// don't know if this is the correct api
window.IMG = function(src, data, cb) {
  this.src = src;
  this.data = data || {};
  this.cb = cb;
  if (this.cb) { this.cb(); }
};

/**
 * @params fList: a list of functions to use as callbacks for tests
 * [{
 *   func: function(o) { return (o === 1); }
 *   arg: 1
 *  }]
 */
var runTests = function(fObj) {
  fObj.forEach(function(value, key, listObj, argument) {
    value.func.apply(window, [value.arg[key]]);
  });
};

/* higher level, application-specific mocks */
/**
 * @author: Whit Waldo
 */
/* Empty function that we'll mock onto later on */
window.utag = {};
window.utag._log = false;
window.utag.view = function() {};
window.utag.link = function() {};
window.utag.handler = {};
window.utag.handler.C = function() {};

//Define some objects and functions that'll be called later on
window.utag_data = {};
window.utag.data = {};
window._cleanDL = function() {
  return {};
};

QUnit.testStart(function() {
  sinon.spy(console, "log"); //Wraps calls to console.log

  //Set a function to _trackAnalytics to cache any inbound events before utag loads up
  window._trackAnalytics = function(obj) {
    if (!obj) {
      return true;
    } //Always return true so the function is defined

    //Log the trackAnalytics request if window._log is set
    if (window["_log"]) {
      if (console.group) {
        console.group("_trackAnalytics Request");
      }
      console.log("Object:");
      console.log(typeof obj === 'object' ? JSON.stringify(obj) : obj);
      console.log("String:");
      console.log(typeof obj === 'object' ? JSON.stringify(obj, null, 4) : obj);
      if (console.groupEnd) {
        console.groupEnd();
      }
    }

    //Check to see if we already have a location to cache the _trackAnalytics call data at
    if (!window._cachedAnalyticsData || typeof window._cachedAnalyticsData !== "object") {
      window._cachedAnalyticsData = [];
    }

    //Cache this value into this array
    window._cachedAnalyticsData.push(obj);
  };
});

QUnit.testDone(function() {
  console.log.restore(); //Unwraps any spies

  //Clear out the _cachedAnalyticsData
  window._cachedAnalyticsData = [];
});

QUnit.module("Mock _trackAnalytics function");

QUnit.test("Log should reflect inbound data if the logging flag is set", function(assert) {
  var testData = {
    site_events: {
      something_happened: true,
      everything_happened: true
    },
    user: "user@example.com"
  };
  window._log = true;

  //Make a test call to _trackAnalytics as defined by the DataLayerReady script
  window._trackAnalytics(testData);

  //Validate that the log function was called
  assert.ok(console.log.callCount > 0, "The spy wasn't ever called");

  //Validate that the _cachedAnalyticsData value is populated and has a length of > 0
  assert.ok(window._cachedAnalyticsData != null, "_cachedAnalyticsData is populated on the window");
  assert.ok(window._cachedAnalyticsData.length > 0, "_cachedAnalyticsData has data stored in its array");
});

QUnit.test("Log shouldn't reflect any inbound data if the logging flag isn't set", function(assert) {
  var testData = {
    site_events: {
      something_happened: true,
      everything_happened: true
    },
    user: "user@example.com"
  };
  window._log = false;

  //Make a test call to _trackAnalytics as defined by the DataLayerReady script
  window._trackAnalytics(testData);

  //Validate that the log function was not called
  assert.ok(console.log.callCount == 0, "console.log was called");

  //Validate that the _cachedAnalyticsData value is populated and has a length of > 0
  assert.ok(window._cachedAnalyticsData != null, "_cachedAnalyticsData is populated on the window");
  assert.ok(window._cachedAnalyticsData.length > 0, "_cachedAnalyticsData has data stored in its array");
});

QUnit.test("If multiple objects are passed into the mock _trackAnalytics, they should be retained in the order sent", function(assert) {
  var testOne = {
    name: "first",
    site_events: {
      something_happened: true
    }
  };
  var testTwo = {
    name: "second",
    site_events: {
      something_happened: true
    }
  };
  var testThree = {
    name: "third",
    site_events: {
      something_happened: true
    }
  };
  window._log = false;

  //Make a call for each of these variables to _trackAnalytics
  window._trackAnalytics(testOne);
  assert.ok(console.log.callCount == 0, "console.log has not been called after the first call");

  window._trackAnalytics(testTwo);
  assert.ok(console.log.callCount == 0, "console.log has not been called after the second call");

  window._trackAnalytics(testThree);
  assert.ok(console.log.callCount == 0, "console.log has not been called after the third call");

  //Validate that the data is stored in the order we'd expect (FILO)
  assert.ok(window._cachedAnalyticsData != null, "_cachedAnalyticsData is populated on the window");
  assert.ok(window._cachedAnalyticsData.length === 3, "_cachedAnalyticsData has the three expected values in here");
  assert.ok(window._cachedAnalyticsData[0].name === "first", "The first data value contains an expected property");
  assert.ok(window._cachedAnalyticsData[1].name === "second", "The second data value contains an expected property");
  assert.ok(window._cachedAnalyticsData[2].name === "third", "The third data value contains an expected property");
});

QUnit.module("Retrieve next playback object");
QUnit.test("Should retrieve objects in the order they're placed in the queue", function(assert) {
  var testOne = {
    name: "first",
    site_events: {
      something_happened: true
    }
  };
  var testTwo = {
    name: "second",
    site_events: {
      something_happened: true
    }
  };
  var testThree = {
    name: "third",
    site_events: {
      something_happened: true
    }
  };
  window._log = false;

  //Make a call to put each of these in the mock _trackAnalytics object
  window._trackAnalytics(testTwo);
  window._trackAnalytics(testOne);
  window._trackAnalytics(testThree);

  //Validate that the data is retrieved in the expected order
  assert.ok(window._cachedAnalyticsData != null, "_cachedAnalyticsData is populated on the window");
  assert.ok(window._cachedAnalyticsData.length === 3, "_cachedAnalyticsData has the three expected values in here");
  var first = window._retrieveNextPlaybackObject();
  assert.ok(first.name === "second"); //We put the second one in first
  var second = window._retrieveNextPlaybackObject();
  assert.ok(second.name === "first"); //We put the first one in second
  var third = window._retrieveNextPlaybackObject();
  assert.ok(third.name === "third"); //We put the third in last
});

QUnit.module("_performDataLayerReady function");
QUnit.test("The _trackAnalytics function should be called when the data layer is signaled as ready", function(assert) {
  var applyFunction = sinon.stub(window, "_applyTrackAnalyticsFunction");
  var replayFunction = sinon.stub(window, "_replayCachedRequests");
  var performFunction = sinon.stub(window, "_performTealiumView");

  //Make a call to the _performDataLayerReady function
  _performDataLayerReady();

  //Assert that each function was actually called (rather, the stub was)
  assert.ok(applyFunction.calledOnce, "The applyTrackAnalyticsFunction function was actually called");
  assert.ok(replayFunction.calledOnce, "The replyFunction was actually called");
  assert.ok(performFunction.calledOnce, "The performFunction was actually called");

  //Unwrap the functions
  _applyTrackAnalyticsFunction.restore();
  _replayCachedRequests.restore();
  _performTealiumView.restore();
});

QUnit.module("_replayCachedRequests function");
QUnit.test("If the data source has one item, a single call to _trackAnalytics should occur", function(assert) {
  //Set the initial data
  var testData = {
    name: "first",
    "site_events": {
      something_happened: true
    }
  };
  window._log = false;

  //Make a call to place this in the queue
  _trackAnalytics(testData);

  //We're not going to use the _dataLayerReady function here, but will call its methods
  //Apply the track analytics method
  _applyTrackAnalyticsFunction();

  //Now, we'll spy on the new _trackAnalytics function
  sinon.spy(window, "_trackAnalytics");

  //We expect that _trackAnalytics will be called one time since there's one data element
  //Next, call the _reply functionality with the location of the stored data
  _replayCachedRequests(window._cachedAnalyticsData);

  assert.ok(window._trackAnalytics.calledOnce === true, "_trackAnalytics was called only a single time");

  //Release the wrapping
  _trackAnalytics.restore();
});

QUnit.test("If there are no cached analytics objects, _trackAnalytics shouldn't be called", function(assert) {
  //Double check that there's no cached data
  window._cachedAnalyticsData = [];

  //We're not going to use the _dataLayerReady function here, but will call its methods
  //Apply the track analytics method
  _applyTrackAnalyticsFunction();

  //Now, we'll spy on the new _trackAnalytics function
  sinon.spy(window, "_trackAnalytics");

  //We expect that _trackAnalytics will be called one time since there's one data element
  //Next, call the _reply functionality with the location of the stored data
  _replayCachedRequests(window._cachedAnalyticsData);

  assert.ok(window._trackAnalytics.called === false, "_trackAnalytics was never called, as expected");

  //Release the wrapping
  window._trackAnalytics.restore();
});

QUnit.test("_trackAnalytics should be called if there are cached analytics objects", function(assert) {
  //Set the initial data
  var testOne = {
    name: "first",
    "site_events": {
      something_happened: true
    }
  };
  var testTwo = {
    name: "second",
    "site_events": {
      something_happened: true
    }
  };
  var testThree = {
    name: "third",
    "site_events": {
      something_happened: true
    }
  };
  window._log = false;

  //Make a call to place this in the queue
  window._trackAnalytics(testOne);
  window._trackAnalytics(testThree);
  window._trackAnalytics(testTwo);

  //We're not going to use the _dataLayerReady function here, but will call its methods
  //Apply the track analytics method
  _applyTrackAnalyticsFunction();

  //Now, we'll spy on the new _trackAnalytics function
  sinon.spy(window, "_trackAnalytics");

  //We expect that _trackAnalytics will be called one time since there's one data element
  //Next, call the _reply functionality with the location of the stored data
  window._replayCachedRequests(window._cachedAnalyticsData);

  assert.ok(window._trackAnalytics.calledThrice === true, "_trackAnalytics was called all three times");

  //Release the wrapping
  window._trackAnalytics.restore();
});

QUnit.module("Validate Tealium exists");
QUnit.test("Should return true when everything is populated", function(assert) {
  //Empty function that we'll mock onto later on
  window.utag = {};
  window.utag.view = function() {};
  window.utag.link = function() {};
  window.utag.handler = {};
  window.utag.handler.C = function() {};

  //Define some objects and functions that'll be called later on
  window.utag_data = {};
  window.utag.data = {};
  window._cleanDL = function() {
    return {};
  };

  var result = validateTealium();

  assert.ok(result === true, "Validate successfully passes");
});

QUnit.test("Should return false when not everything is populated", function(assert) {
  //Throw a null in the arguments
  window.utag.handler.C = null;

  var result = validateTealium();

  assert.ok(!result, "Validate successfully fails");

  //Restore the function
  window.utag.handler.C = function() {};
});