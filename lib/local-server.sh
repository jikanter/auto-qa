#!/bin/bash

# Utility to run from the local machine
SELENIUM_SERVER=$(which selenium-server)
if [ "${SELENIUM_SERVER}" != "" ]; then
  echo "running local selenium server"
  ${SELENIUM_SERVER} -debug true
else
  echo "you must have selenium-server in your path to run this" && exit -1;
fi